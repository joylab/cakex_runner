<?php
use PHPUnit\Framework\TestCase;
require( __DIR__ . './../vendor/autoload.php' );

/**
 * PHPUnit tests for user input validation
 */
class ValidationTests extends TestCase {
  /**
   * Not Empty Validation for user input
   */
  public function testNotEmptyValidation() {
		$V = new Validation();

		$input = 'aaa';
		$expected = true;
		$result = $V->notEmptyValidation($input);
    $this->assertEquals($expected, $result);

		$input = null;
		$expected = false;
		$result = $V->notEmptyValidation($input);
    $this->assertEquals($expected, $result);

		$input = 'false';
		$expected = true;
		$result = $V->notEmptyValidation($input);
    $this->assertEquals($expected, $result);
  }

  /**
   * Email Validation for user input
   */  
  public function testEmailValidation() {
    $V = new Validation();
  
    $email = 'a@a.a';
    $expected = 'a@a.a';
		$result = $V->EmailValidation($email);
    $this->assertEquals($expected, $result);
    
    $email = 'a@a.';
    $expected = null;
		$result = $V->emailValidation($email);
    $this->assertEquals($expected, $result);
  }
  /**
   * Timezone Validation for user input
   */
  public function testTimezoneValidation() {
    $V = new Validation();
    $timezone_list = timezone_identifiers_list();
    
    $timezone = 'Asia/Beirut';
    $expected = true;
		$result = $V->timezoneValidation($timezone);
    $this->assertEquals($expected, $result);
    
    $timezone = 'aaa/bbb';
    $expected = false;
		$result = $V->timezoneValidation($timezone);
    $this->assertEquals($expected, $result);
  }
  /**
   * Select Validation for user input
   * @dataProvider dataProvider
   */
  public function testSelectValidation($choices) {
    $V = new Validation();
    
    $selection = 2;
    $expected = true;
		$result = $V->selectValidation($selection, $choices);
    $this->assertEquals($expected, $result);
    
    $selection = 4;
    $expected = false;
		$result = $V->selectValidation($selection, $choices);
    $this->assertEquals($expected, $result);
  }
  /**
   * Data Provider for select testing
   */  
  public function dataProvider() {
    return [
      [[1 => 'en', 2 => 'fr', 3 => 'ar']]
    ];
  }
  
}
?>
