<?php

## Runner command configs
#########################################
define('AVAILABLE_COMMANDS', [
	[
		'name' => 'cake',
		'description' => 'Call cake commands',
	],
	[
		'name' => 'create',
		'description' => 'Create cakex project',
	],
	[
		'name' => 'edit',
		'description' => 'Edit cakex project variables',
	],
	[
		'name' => 'setup',
		'description' => 'Setup a new cakex project',
	],
]);

define('COMMANDS_ARGUMENTS', [
	[
		'command' => 'create',
		'name' => 'project-name',
		'description' => 'Enter cakex project name to create',
	],
	[
		'command' => 'edit',
		'name' => 'variable-to-edit',
		'description' => 'Enter variable that you want to edit',
	],
	[
	  'command' => 'cake',
		'name' => 'cake-command',
		'description' => 'Enter cake command to call',
	],
]);

define('COMMANDS_OPTIONS', [
	[
		'command' => 'create',
		'name' => 'directory-path',
		'description' => 'Enter the path of the directory',
		'short' => 'p',
	],
]);

define('DEFAULT_DIRECTORY', '/home/abed/repos/');

define('TEMPLATE_CAKEX', '/home/abed/repos/cakex');

## Setup command configs
#########################################


define('PROJECT_FILES',
        [
         'core' =>
         [
          'name' =>'core.php',
          'path' => '/app/config/',
          'variables' => 
          [
            'Timezone' =>
            [
              'name' => 'Timezone',
              'info' => 
              [
                'heading' => 'Available timezone choices:',
                'choices' =>
                [
		              1 => 'Asia/Beirut',
		              2 => 'America/Montreal',
		              3 => 'America/New_York',
		              4 => 'Asia/Dubai',
		              5 => 'Europe/London',
		              6 => 'Other Timezone',
	              ],
	              'other_choice_number' => '6',
              ],
            ],
            'AdminEmail' => 
            [
              'name' => 'AdminEmail',
              'info' => 
              [
                'heading' => 'Enter Admin Emails.',
              ]
            ],
            'Language' =>
            [
              'name' => 'Language',
              'info' =>
              [
                'choices' => 
                [
		              1 => 'en',
		              2 => 'fr',
		              3 => 'ar',
	              ],
              ],
            ],
          ],
        ],
        'database' =>
        [
          'name' =>'database.php',
          'path' => '/app/config/',
          'variables' =>
          [
            'DatabaseHost' => 
            [
              'name' => 'DatabaseHost',
            ],
            'DatabaseLogin' => 
            [
              'name' => 'DatabaseLogin',
            ],
            'DatabasePassword' => 
            [
              'name' => 'DatabasePassword',
            ],
            'DatabaseName' => 
            [
              'name' => 'DatabaseName',
            ],
          ],
        ],
      ],
    );

define('VARIABLES', [
						[
							'name' =>'ProjectName',
							'filename' => '',
							'path' => '',
							'type' => 'string',
							'select' => false,
						],
						[
							'name' =>'Timezone',
							'filename' => 'core',
							'path' => '/app/config/core.php',
							'type' => 'string',
							'select' => true,
							'substr_extension' => "');//Timezone",
						],
						[
							'name' =>'AdminEmail',
							'filename' => 'core',
							'path' => '/app/config/core.php',
							'type' => 'array',
							'select' => false,
							'substr_extension' => "']);//[AdminEmail]",
						],
						[
							'name' =>'Language',
							'filename' => 'core',
							'path' => '/app/config/core.php',
							'type' => 'array',
							'select' => true,
							'substr_extension' => "']);//[Language]",
						],
						[
							'name' =>'DatabaseHost',
							'filename' => 'database',
							'path' => '/app/config/database.php',
							'type' => 'string',
							'select' => false,
							'substr_extension' => "',//DatabaseHost",
						],
						[
							'name' =>'DatabaseLogin',
							'filename' => 'database',
							'path' => '/app/config/database.php',
							'type' => 'string',
							'select' => false,
							'substr_extension' => "',//DatabaseLogin",
						],
						[
							'name' =>'DatabasePassword',
							'filename' => 'database',
							'path' => '/app/config/database.php',
							'type' => 'string',
							'select' => false,
							'substr_extension' => "',//DatabasePassword",
						],
						[
							'name' =>'DatabaseName',
							'filename' => 'database',
							'path' => '/app/config/database.php',
							'type' => 'string',
							'select' => false,
							'substr_extension' => "',//DatabaseName",
						],
      ]);
      
define('VARIABLES_TO_EDIT',
       [
		     1 => 'Timezone',
		     2 => 'AdminEmail',
		     3 => 'Language',
		     4 => 'DatabaseHost',
		     5 => 'DatabaseLogin',
		     6 => 'DatabasePassword',
		     7 => 'DatabaseName',
		   ]
		 ); 
      
      
      
      
