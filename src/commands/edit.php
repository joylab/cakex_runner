<?php

require_once __DIR__ . '/../../config.php';
require_once __DIR__ . '/../../vendor/autoload.php';

/**
 * Edit command
 */
class Edit {

 /**
 	* Class constructor
  */
	public function __construct($variable_to_edit = null) {
	  $Log = new CakexLog();
    $this->project_path = $Log->getLog();
    $this->variable_to_edit = $variable_to_edit;
  }
  
 /**
  * Edit project starter function
  */
  public function editProject() {
    $IC = new InputCapture();
    $FM = new FileManipulation();
    if ($FM->verifyProject($this->project_path) && $this->verifyVariableToEdit() != null) {
      $variable_info = $this->verifyVariableToEdit();
      if ($variable_info['path'] == null) {
        $this->editProjectName();
      }
      else {
        $old_value = $FM->getOldValue($this->project_path, $variable_info['path'], $variable_info['name'], $variable_info['type']);
        Out::echoNormal("Old Value(s): '".$old_value."'\n");
        if ($variable_info['select']) {
          if ($variable_info['type'] == 'string') {
            $config_info = PROJECT_FILES[$variable_info['filename']]['variables'][$variable_info['name']]['info'];
            $response = $IC->captureMultiSelect($variable_info['name'], $config_info['heading'], $config_info['choices'],
	                                              $variable_info['name'], $config_info['other_choice_number']);
          }
          else {
            $config_info = PROJECT_FILES[$variable_info['filename']]['variables'][$variable_info['name']]['info'];
            $response = $IC->captureMultiInputSelect($variable_info['name'], $config_info['choices'], false);
          }
        }
        else {
          if ($variable_info['type'] == 'string') {
            $response = $IC->captureSingleInput($variable_info['name'], false);
          }
          else {
            $config_info = PROJECT_FILES[$variable_info['filename']]['variables'][$variable_info['name']]['info'];
            $response = $IC->captureMultiInput($config_info['heading'], $variable_info['name'], false, $variable_info['name']);
          }
        }
        $new_value = $FM->setGeneratedValue($response);
        $FM->strReplacement($this->project_path.$variable_info['path'],
                            $old_value.$variable_info['substr_extension'],
                            $new_value.$variable_info['substr_extension']);
        Out::echoNormal("The ".$variable_info['name']. " is edited successfully for ".$this->project_path. " project.\n"
                        ."Old Value: '".$old_value. "' --> New Value: '".$new_value."'\n");
        }
    }
    else {
      Out::echoError('Invalid variable to edit, here is the variables list.');
	    Out::echoValidInput(VARIABLES_TO_EDIT);
    }
  }
  
 /**
  * Check if the variable_to_edit is acceptable
  */
  public function verifyVariableToEdit() {
    foreach(VARIABLES as $editable_variable) {
      if (strcasecmp($editable_variable['name'], $this->variable_to_edit) == 0) {
        $this->variable_to_edit = $editable_variable['name'];
        return $editable_variable;
      }
    }
    return false;
  }
    
 /**
  * Edit project name
  */
  public function editProjectName() {
    $new_name = $IC->captureSingleInput('ProjectName', false);
    $new_project_path = $FM->getNewProjectPath($this->project_path, $new_name);
    if ($FM->verifyProject($new_project_path)) {
      Out::echoError('Invalid project name input, directory already exists.');
  	}
  	else {
  	  $old_project_path = $this->project_path;
      $this->project_path = $FM->renameProject($old_project_path, $new_project_path);
      $Log = new CakexLog();
      $Log->clearLog();
      $Log->writeInfoLog($new_project_path);
      Out::echoNormal("The ".$old_project_path." project is renamed to ".$this->project_path.".\n");
    }
  } 
}
