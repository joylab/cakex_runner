<?php

require_once __DIR__ . '/../../config.php';
require_once __DIR__ . '/../../vendor/autoload.php';

define('BAKE_COMMANDS' , [ 'db_config', 'model', 'view', 'controller', 'project', 'fixture', 'test', 'plugin', 'all']);

/**
 * Bake command
 */
class Bake {

 /**
 	* Class constructor
  */
	public function __construct($project_path = null, $bake_arguments = []) {
	  $this->bake_arguments = $bake_arguments;
    $this->project_path = $project_path;
  }
  
 /**
  * bake command call
  */
  public function bakeCall() {
    $app_full_path = '-app '.$this->project_path.'/app';
    if (empty($this->bake_arguments)) {
      passthru($this->project_path.'/cake/console/cake bake '.$app_full_path);
    }
    else {
      if ($this->isBakeCommand()) {
        $arguments = implode(" ", $this->bake_arguments);
        $full_command = $this->project_path.'/cake/console/cake bake '.$app_full_path. ' '.$arguments;
        passthru($full_command);
      }
      else {
        Out::echoError('Invalid bake command, please try again.');
        Out::echoNormal("Here's the complete bake commands list:\n");
        $this->displayBakeCommands();
        }
    }
  }
  
   /**
  * Check is the command is a valid bake command
  */
  public function isBakeCommand() {
    foreach (BAKE_COMMANDS as $bake_command) {
      if (strcasecmp($this->bake_arguments[0], $bake_command) == 0) {
        $this->bake_arguments[0] = $bake_command;
        return true;
      }
    }
    return false;
  }
  
   /**
  * Check is the command is a valid bake command
  */
  public function displayBakeCommands() {
    Out::echoValidInput(BAKE_COMMANDS);
  }
  
  
}
