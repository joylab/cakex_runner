<?php

require_once __DIR__ . '/../../config.php';
require_once __DIR__ . '/../../vendor/autoload.php';

/**
 * Create command
 */
class Create {
 /**
 	* Class constructor
  */
	public function __construct($directory_path = null, $project_name = null) {
	  $this->directory_path = $directory_path;
  	$this->project_name = $project_name;
  }
  
 /**
  * The cakex project creator function
  */
  public function createCakex() {
  	$FM = new FileManipulation();
  	if ($FM->verifyProject($this->directory_path.'/'.$this->project_name)) {
  		Out::echoError('Error, the project: '.$this->project_name. ' from directory '.$this->directory_path.' already exists.');
  		die();
  	}
		$project_path = preg_replace('#/+#','/', $this->directory_path.'/'.$this->project_name);
  	$FM->generateProjectFolder($project_path);
    $FM->generateCakeFolder($project_path);
    $FM->extractAppSample($project_path);
    $FM->extractIndexFile($project_path);
    $Log = new CakexLog();
		$Log->clearLog();
    $Log->writeInfoLog($project_path);
    Out::echoNormal("The project: ".$this->project_name. ' is created successfully in directory: '.$this->directory_path."\n");
  }
  
}
