<?php

require_once __DIR__ . '/../../config.php';
require_once __DIR__ . '/../../vendor/autoload.php';

define('CAKE_COMMANDS' , [ 'acl', 'bake', 'i18n', 'testsuite', 'api', 'console', 'schema']);

/**
 * Cake command
 */
class Cake {

 /**
 	* Class constructor
  */
	public function __construct($cake_arguments = []) {
	  $this->cake_arguments = $cake_arguments;
	  $Log = new CakexLog();
    $this->project_path = $Log->getLog();
  }
  
 /**
  * cake command call
  */
  public function cakeCall() {
    if (empty($this->cake_arguments)) {
      passthru($this->project_path.'/cake/console/cake');
    }
    else {
		  if($this->isCakeCommand() && $this->cake_arguments[0] == 'bake') {
		    $bake_arguments = $this->cake_arguments;
		    array_shift($bake_arguments);
		    $Bake = new Bake($this->project_path, $bake_arguments);
		    $Bake->bakeCall();
		  }
		  else {
		    if ($this->isCakeCommand()) {
		      $arguments = implode(" ", $this->cake_arguments);
          $full_command = $this->project_path.'/cake/console/cake '.$arguments;
          passthru($full_command);
        }
        else {
          Out::echoError('Invalid cake command, please try again.');
          Out::echoNormal("Here's the complete cake commands list:\n");
          $this->displayCakeCommands();
        }
		  }
    }
  }
  
 /**
  * Check is the command is a valid cake command
  */
  public function isCakeCommand() {
    foreach (CAKE_COMMANDS as $cake_command) {
      if (strcasecmp($this->cake_arguments[0], $cake_command) == 0) {
        $this->cake_arguments[0] = $cake_command;
        return true;
      }
    }
    return false;
  }
  
   /**
  * Check is the command is a valid cake command
  */
  public function displayCakeCommands() {
    Out::echoValidInput(CAKE_COMMANDS);
  }
  
}
