<?php

require_once __DIR__ . '/../../config.php';
require_once __DIR__ . '/../../vendor/autoload.php';

/**
 * Setup command
 */
class Setup {
 /**
  * The setup command launcher function
  */
  public function launch() {
    $database = [
		  'DatabaseHost'=> 'localhost',
		  'DatabaseLogin'=> 'abed',
		  'DatabasePassword'=> 'abed123',
		  'DatabaseName'=> 'abed-myCakeProject'
		];
    $database = $this->setupDatabase();
    
    $timezone = 'Asia/Beirut';
    $timezone = $this->setupTimezone();
    
    $emails = ['joy@joylab.ca', 'abed@gmail.com'];
    $emails = $this->setupAdminEmails();
    
    $languages = ['en', 'ar'];
    $languages = $this->setupLanguages();
    
    $user_inputs = [
      'Database' => $database,
      'Timezone' => $timezone,
      'AdminEmail' => $emails,
      'Language' => $languages,
    ];
    
    $this->setupProject($user_inputs);
  }
 
 /**
  * Setup all database variables
  */
	public function setupDatabase() {
	  $IC = new InputCapture();
	  $database = [
		  'DatabaseHost'=> '',
		  'DatabaseLogin'=> '',
		  'DatabasePassword'=> '',
		  'DatabaseName'=> ''
		];
		
	  foreach ($database as $database_name => $database_value) {
		  $database[$database_name] = $IC->captureSingleInput($database_name, false);
		  echo 'Your ' . $database_name . ' is: ' . $database[$database_name] . "\n";
	  }
	  return $database;
	}

 /**
  * Setup timezone
  */
	public function setupTimezone() {
	  $IC = new InputCapture();
	  $timezone = $IC->captureMultiSelect('Timezone', PROJECT_FILES['core']['variables']['Timezone']['info']['heading'],
	                                      PROJECT_FILES['core']['variables']['Timezone']['info']['choices'], 'Timezone',
	                                      PROJECT_FILES['core']['variables']['Timezone']['info']['other_choice_number']);
	  echo 'Your Timezone is: ' . $timezone . "\n";
	  return $timezone;
	}

 /**
  * Setup admin emails
  */
	public function setupAdminEmails() {
	  $IC = new InputCapture();
	  $emails = $IC->captureMultiInput(PROJECT_FILES['core']['variables']['AdminEmail']['info']['heading'], 'AdminEmail', false, 'AdminEmail');
	  echo "Your Admin Email(s):\n";
	  print_r($emails);
	  return $emails;
	}
	
 /**
  * Setup languages
  */
	public function setupLanguages() {
	  $IC = new InputCapture();
	  $languages = $IC->captureMultiInputSelect('Language', PROJECT_FILES['core']['variables']['Language']['info']['choices'], false);
	  echo "Your Language(s):\n";
	  print_r($languages);
	  return $languages;
	}
	
 /**
  * Setup project using FileManipulation class
  */
  public function setupProject($user_inputs = []) {
    $Log = new CakexLog();
    $project_path = $Log->getLog();
    $FM = new FileManipulation();
    $detailed_user_inputs = $FM->extendedUserInput($user_inputs);
    foreach ($detailed_user_inputs as $variable_name => $user_input) {
      $variable_info = $this->getVariableInfo($variable_name);
      $old_value = $FM->getOldValue($project_path, $variable_info['path'], $variable_name, $variable_info['type']);
      $new_value = $FM->setGeneratedValue($user_input);
      $FM->strReplacement($project_path.$variable_info['path'], $old_value, $new_value);
    }
    Out::echoNormal("The project: ".$project_path. " is set up successfully with all inputs you entered.\n");
	}
	
 /**
  * Get information about a variable
  */
  public function getVariableInfo($variable) {
    foreach(VARIABLES as $config_variable) {
      if ($config_variable['name'] == $variable) {
        return $config_variable;
      }
    }
    return false;
  }
}
