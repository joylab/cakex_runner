<?php
/**
 * User input validation
 */
class Validation {
  
   /**
	  * User input validation for not empty
	  */
	public function notEmptyValidation($input_value = null) {
	  $result = false;
		if (!empty($input_value) && !ctype_space($input_value)) {
		  $result = true;
		}
		return $result;
	}

  /**
	 *  User input validation for email
	 */
	public function emailValidation($email = null) {
		$result = filter_var($email, FILTER_VALIDATE_EMAIL);
		return $result;
	}

	/**
	 *  User input validation for timezone
   */
	public function timezoneValidation($timezone = null) {
		$timezone_list = timezone_identifiers_list();
		$result = false;
		foreach ($timezone_list as $timezone_nb => $timezone_value) {
			if ($timezone_value == $timezone) {
				$result = true;
			}
		}
		return $result;
	}
	
	/**
	 *  User input validation for selection
	 */
	public function selectValidation($selection = null, $choices = []) {
		foreach ($choices as $input_value => $value) {
		  if ($selection == $input_value) {
			  return true;
			}
		}
		return false;
	}
}
