<?php
/**
 * Outputting based on colors
 */
class Out {

  static $colors = [
    'dark_gray'    => '30',
    'blue'         => '34',
    'green'        => '32',
    'cyan'         => '36',
    'red'          => '31',
    'purple'       => '35',
    'yellow'       => '33',
    'white'        => '37',
    'normal'       => '39',
  ];

  /**
   * Basic Output method.
   */
  public static function echo($string = null, $color = null) {
    $normal_color = self::$colors['normal'];
    $color_code = 0; 
    foreach (self::$colors as $col => $code) {
      if ($col == $color) {
        $color_code = $code;
      }
    }
    echo "\e[".$color_code."m".$string."\e[".$normal_color."m";
  }

  /**
   * Invalid Output in red color.
   */
  public static function echoNormal($normal = null) {
    $string = $normal;
    return self::echo($string, 'brown');
  }

	/**
   * Invalid Output in red color.
   */
  public static function echoError($error = null) {
    $string = $error."\n";
    return self::echo($string, 'red');
  }

  /**
   * Warning Output in blue color.
   */
  public static function echoWarning($warning = null) {
    $string = $warning."\n";
    return self::echo($string, 'blue');
  }

  /**
   * Success Output in green color.
   */
  public static function echoSuccess($success = null) {
    $string = $success."\n";
    return self::echo($string, 'green');
  }

  /**
   * Invalid Output return echoError
   */
  public static function echoInvalid($input_name = null, $output = null) {
    $string = null;
    if ($output == null) {
      $string = 'Empty input is invalid for'.$input_name.', please try again';
    }
    else {
      $string = 'Invalid input ('.$output.') for '.$input_name.', please try again';
    }
    return self::echoError($string);
  }

  /**
   * Heading Output in yellow color.
   */
  public static function echoHeading($heading = null) {
    $string = $heading."\n";
    return self::echo($string, 'yellow');
  }

  /**
   * Output selection choices 
   */
  public static function echoSelection($choices = []) {
    $string = null;
    foreach ($choices as $input_value => $value) {
		  $string .= $input_value.'. '.$value."\n";
		}
		return self::echo($string, 'cyan');
  }
  
    /**
   * Output valid input
   */
  public static function echoValidInput($input_list = []) {
    $string = null;
    foreach ($input_list as $input_name => $value) {
		  $string .= '- '.$value."\n";
		}
		return self::echo($string, 'cyan');
  }
}
