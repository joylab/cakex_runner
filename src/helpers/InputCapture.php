<?php
require __DIR__ . '/../../vendor/autoload.php';

/**
 * capture user input based on type and quantity
 */
class InputCapture {

	/**
 	 * Class constructor to instantiate the validator
   */
	public function __construct() {
  	$this->Validator = new Validation();
  }
  
  /**
 	 * Return single user input with an option for empty input
   */
  public function captureSingleInput($input_name = null, $allowEmpty = false) {
     $input_value = readline(Out::echoNormal('Enter ' . $input_name . ': '));
     if (!$allowEmpty) {
      if ($this->Validator->notEmptyValidation($input_value) == null) {
     	  Out::echoInvalid($input_name, $input_value);
     	  $input_value = $this->captureSingleInput($input_name, $allowEmpty);
     	}
		 }
     return $input_value;
  }
  
  /**
	 * Multiple call for captureSingleInput() with validation type option
	 */
	public function captureMultiInput($heading = null, $input_name = null, $allowEmpty = false, $validation_type = null) {
	  $multi_input_heading = 'Enter one '.$input_name.' per line and Press (.) if finished.';
		$input = null;
		$multi_values = array();
		## Display Heading
		Out::echoHeading($heading);
	  Out::echoHeading($multi_input_heading);
		## Loop on input lines under terminal character inputted
		$terminal_character = '.';

		while ($input !== $terminal_character) {
			$input = $this->captureSingleInput($input_name, $allowEmpty);
			if ($input !== $terminal_character) {
				if ($validation_type === 'AdminEmail') {
					if ($this->Validator->emailValidation($input)) {
						array_push($multi_values, $input);
					}
					else {
     	      Out::echoInvalid($input_name, $input);
					}
				}
				else {
					array_push($multi_values, $input);
				}
			}
			else {
			  if (!$allowEmpty && empty($multi_values)) {
			    $input = null;
			    Out::echoWarning('You have to choose at least one '.$input_name);
		    }
			}
		}
		Out::echoSuccess($input_name. ' selected successfully.');
		return $multi_values;
	}
	
  /**
	 * Return a selected choice from the choices array
   */
	public function captureMultiSelect($input_name = null, $heading = null, $choices = [], $validation_type = null, $other_choice_number) {
		$selection = null;
		$valid_selection = null;

		while (empty($valid_selection)) {
			Out::echoHeading($heading);
			Out::echoSelection($choices);
			$selection = $this->captureSingleInput('selection', false);
			
			if ($this->Validator->selectValidation($selection, $choices) == null) {
			  Out::echoInvalid('selection', $selection);
			}
			else {
			  $valid_selection = $selection;
			  $input_value = $choices[$valid_selection];
			  if (!empty($other_choice_number) && $valid_selection == $other_choice_number) {
			    $input_value = $this->captureSingleInput($input_name, false);
			    if ($validation_type === 'Timezone' && !$this->Validator->timezoneValidation($input_value)) {
			      Out::echoInvalid($input_name, $input_value);
					  $valid_selection = null;
			    }
			  }
			}
		}
    Out::echoSuccess($input_name. ' selected successfully.');
		return $input_value;
	}
	/**
	 *
   */
		public function captureMultiInputSelect($input_name = null, $choices = [], $allowEmpty = false) {
      $multi_input_heading = 'Enter one '.$input_name.' per line and Press (.) if finished.';
		  $input_value = null;
		  $multi_values = array();
		  ## Display Heading
	    Out::echoHeading($multi_input_heading);
		  ## Loop on input lines under terminal character inputted
		  $terminal_character = '.';

		  while ($input_value !== $terminal_character) {
			  
			  $selection = null;
		    $valid_selection = null;

			  while (empty($valid_selection)) {
			    Out::echoSelection($choices);
			    $selection = $this->captureSingleInput('selection', false);
			    if ($selection == $terminal_character) {
			      if (!$allowEmpty && empty($multi_values)) {
			        Out::echoWarning('You have to choose at least one '.$input_name);
			        break;
			      }
			      else {
			        Out::echoSuccess($input_name. ' selected successfully.');
			        return $multi_values;
			      }
			    }
			     
			    if ($this->Validator->selectValidation($selection, $choices) == null) {
			      Out::echoInvalid('selection', $selection);
			    }
			    else {
			      $valid_selection = $selection;
			      $input_value = $choices[$valid_selection];
			      array_push($multi_values, $input_value);
            $i = null;
            $count = count($choices);
            for ($i = $valid_selection; $i<$count; $i++) {
              $choices[$i] = $choices[$i+1];
            }
			      unset($choices[$count]);
			      if (empty($choices)) {
			        Out::echoSuccess('All '.$input_name. ' choices are selected successfully.');
			        return $multi_values;
			      }
			    }
		    }  
			}
		  return $multi_values;
	  }
    
}
