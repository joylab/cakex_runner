<?php
use GuzzleHttp\Client;
/**
 * HTTP Client
 */
class CURL {

  /**
   * GET request testing
   */
  public function GuzzleGETTesting() {
    $client = new Client(['base_uri' => 'https://reqres.in/']);
    $response = $client->request('GET', '/api/users?page=10');      
    echo $response->getBody();
  }
  
  /**
   * POST request testing
   */
  public function GuzzlePOSTTesting() {
    $client = new Client(['base_uri' => 'https://reqres.in/']);
    $response = $client->request('POST', '/api/users', ['form_params' => [
      'name' => 'Dan',
      'job' => 'Full Stack Dev'
    ]]);
    echo $response->getBody();
  }
}
