<?php
require __DIR__ . '/../../vendor/autoload.php';
use Monolog\Processor\WebProcessor;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Project logging
 */
class CakexLog {
 /**
  * Write log of INFO intensity Level
  */
  public function writeInfoLog($message = null) {
    $logger = new Logger('default');
    $logger->pushHandler(new StreamHandler(__DIR__ . '/../../app.log', Logger::INFO));
    $logger->pushProcessor(new WebProcessor());
    $logger->info($message);
  }
  
 /**
  * Write log of INFO intensity Level
  */
  public function getLog() {
    $result = null;
    $file_content = file_get_contents('./app.log');
    $result = explode(" ",$file_content);
    return $result[2];
  }
  
  /**
  * Clear log file
  */
  public function clearLog() {
  	file_put_contents('./app.log', null);
  }
  
}
