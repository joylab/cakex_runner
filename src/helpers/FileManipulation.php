<?php
require_once __DIR__ . './../../config.php';
/**
 * File Manipulation method to manage folders and files
 */
class FileManipulation {
  
 /**
  * Generate project folder
  */
  public function generateProjectFolder($project_path = null) {
    mkdir($project_path);
  }
  
  /**
   * Generate cake folder
   */
  public function generateCakeFolder($project_path = null) {
    $this->recurseCopy(TEMPLATE_CAKEX, $project_path.'/cake');
    chmod($project_path.'/cake/console/cake', 0766);
    mkdir($project_path.'/cake/console/templates/skel/tmp');
    mkdir($project_path.'/cake/console/templates/skel/tmp/cache/');
  }
  
 /**
  * Extract app sample
  */
  public function extractAppSample($project_path = null) {
    $cakex_sample_app_path = $project_path.'/cake/samples/sample_app';
    $this->recurseCopy($cakex_sample_app_path, $project_path.'/app');
  }
  
 /**
  * Extract app sample
  */
  public function extractIndexFile($project_path = null) {
    $cakex_sample_index_path = $project_path.'/cake/samples/sample_index.php';
    $extracted_index_path = $project_path.'/index.php';
    copy($cakex_sample_index_path, $extracted_index_path); 
  }
   
  /**
	 * resursive function for copy folders and files
	 */
  public function recurseCopy($src, $dst, $childFolder = null) {
    $dir = opendir($src); 
    mkdir($dst);
    if ($childFolder != null) {
      mkdir($dst.'/'.$childFolder);
      while(false !== ($file = readdir($dir))) { 
        if (($file != '.') && ($file != '..')) { 
          if (is_dir($src.'/'.$file)) { 
            $this->recurseCopy($src.'/'.$file, $dst.'/'.$childFolder.'/'.$file); 
          } 
          else { 
            copy($src.'/'.$file, $dst.'/'.$childFolder.'/'.$file); 
          }  
        } 
      }
    }
    else {
      // return $cc; 
      while(false !== ($file = readdir($dir))) { 
        if (($file != '.') && ($file != '..')) { 
          if (is_dir($src.'/'.$file)) { 
            $this->recurseCopy($src.'/'.$file, $dst.'/'.$file); 
          } 
          else { 
            copy($src.'/'.$file, $dst.'/'.$file); 
          }  
        } 
      } 
    }
    closedir($dir); 
  }

  /**
   * User Input Replacement
   */
  public function userInputReplace($userInput = []) {
    $project_path = './'.$userInput['Project Name']['generated_value'];
    $detailed_userInput = $this->extendedUserInput($userInput);
    foreach ($detailed_userInput as $input => $value) {
      foreach(PROJECT_FILES as $file_key => $file_value) {
        if (isset($file_value['variables'][$input])) {
            $generated_value = $this->setGeneratedValue($value['generated_value']);
	          $this->strReplacement($project_path.$file_value['path'].$file_value['name'], $value['temp_value'], $generated_value);
	      }
	    }
    }
  }

  /**
   * Replace a string with another one in a file
   */
  public function strReplacement($file_path = null, $old_str = null, $new_str = null) {
    $file_content = file_get_contents($file_path);
    $new_file_content = str_replace($old_str, $new_str, $file_content);
    file_put_contents($file_path, $new_file_content);
  }
 
 /**
  * Extend userInput array
  */
  public function extendedUserInput($userInput = []) {
    $detailed_userInput = $userInput;
    foreach ($userInput as $input => $value) {
      if (is_array($value) && !array_key_exists("0", $value)) {
        foreach ($value as $key =>$val) {
          $detailed_userInput[$key] = $val;
        }
        unset($detailed_userInput[$input]);
      }
    }
    return $detailed_userInput;
  }
 /**
   * Make sure the generated value is a string
   */
  public function setGeneratedValue($value) {
    $generated_value = $value;
    if (is_array($generated_value)) {
      $generated_value = implode("', '",$generated_value);
    }
    return $generated_value;
  }
  
 /**
  * check if project exists or no
  */
  public function verifyProject($project_path = null) {
    if (file_exists($project_path)) {
      return true;
    }
    return false;
  }
  
 /**
  * Rename Project
  */
  public function renameProject($old_project_path = null, $new_project_path = null) {
    rename($old_project_path, $new_project_path);
    return $new_project_path;
  }
  
 /**
  * Get the new project path
  */
  public function getNewProjectPath($old_project_path = null, $new_project_name = null) {
  	$old_project_name = basename($old_project_path);
  	$new_project_path = dirname($old_project_path).'/'.$new_project_name;
    return $new_project_path;
  }
  
  public function getOldValue($project_path = null, $variable_file_path = null, $variable_name = null, $variable_type = null) {
    $file_content = file_get_contents($project_path.$variable_file_path);
    if ($variable_type == 'string') {
      $file_content_array = explode("'",$file_content);
      $variable_name_array = preg_grep('/'.$variable_name.'\s.*/', $file_content_array);
      $variable_name_key = array_keys($variable_name_array);
      $variable_value_key = $variable_name_key[0] - 1;
      $variable_value = $file_content_array[$variable_value_key];
      return $variable_value;
    }
    else {
      preg_match_all("/\[([^\]]*)\]/", $file_content, $result);
      $variable_name_key = array_search($variable_name, $result[1]);
      $full_variable_value = $result[1][$variable_name_key-1];
      $full_variable_length = strlen($full_variable_value);
      $last_char_index = $full_variable_length - 2;
      $variable_value = substr($full_variable_value, 1, $last_char_index);
      return $variable_value;
    }
  }

}
